package me.jmnw.otp.util;

import java.io.*;

/**
 * bmessj (19.11.2014).
 */
public class BinaryUtils {

    public File                 file;
    public FileInputStream      fis;
    public BufferedInputStream  bis;
    public FileOutputStream     fos;
    public BufferedOutputStream bos;
    public Mode                 mode;
    public long pos = 0;

    public BinaryUtils(File file, Mode mode) {
        this.file = file;
        this.mode = mode;

        if (!file.exists()) try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (mode == Mode.READ) {
            try {
                fis = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                throw new RuntimeException("File not found!");
            }
            bis = new BufferedInputStream(fis);
        } else {
            try {
                fos = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                throw new RuntimeException("File not found!");
            }
            bos = new BufferedOutputStream(fos);
        }
    }

    public BinaryUtils(String file, Mode mode) {
        this(new File(file), mode);
    }

    public void closeFile() {
        if (mode == Mode.READ) {
            try {
                bis.close();
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                bos.close();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean writeBytes(byte[] bytes) {
        try {
            bos.write(bytes);
            return true;
        } catch (IOException e) {
            throw new RuntimeException("IOException while writing (BinaryUtils.java)");
        }
    }

    public int getByte() {
        try {
            return bis.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }


    public enum Mode {
        READ, WRITE
    }

}
