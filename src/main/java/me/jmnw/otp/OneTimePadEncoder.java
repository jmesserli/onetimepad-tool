package me.jmnw.otp;

import me.jmnw.otp.util.BinaryUtils;

import java.io.File;
import java.util.Random;

/**
 * bmessj (19.11.2014).
 */
public class OneTimePadEncoder {
    public File        key;
    public File        value;
    public File        input;
    public BinaryUtils binUtils_key;
    public BinaryUtils binUtils_value;
    public BinaryUtils binUtils_input;
    public Random random             = new Random();
    public long   processedByteCount = 0L;

    public OneTimePadEncoder(File key, File value, File input) {
        this.key = key;
        this.value = value;
        this.input = input;
        binUtils_key = new BinaryUtils(key, BinaryUtils.Mode.WRITE);
        binUtils_value = new BinaryUtils(value, BinaryUtils.Mode.WRITE);
        binUtils_input = new BinaryUtils(input, BinaryUtils.Mode.READ);
    }

    private byte[] encode(byte in) {
        processedByteCount++;
        byte[] ret = new byte[2];
        byte randomByte = (byte) (in > 0 ? random.nextInt(in) : 0);

        ret[0] = (byte) (in - randomByte);
        ret[1] = randomByte;

        if (processedByteCount % (1048576) == 0)
            System.out.println("Encoded: " + processedByteCount / 1048576 + " MiB");

        return ret;
    }

    public void encodeAll() {
        int in = binUtils_input.getByte();
        while (in > -1) {
            byte[] encoded = encode((byte) in);
            binUtils_key.writeBytes(new byte[]{encoded[0]});
            binUtils_value.writeBytes(new byte[]{encoded[1]});
            in = binUtils_input.getByte();
        }
        binUtils_input.closeFile();
        binUtils_key.closeFile();
        binUtils_value.closeFile();
    }
}
