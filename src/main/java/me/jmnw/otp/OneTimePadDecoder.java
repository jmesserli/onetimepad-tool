package me.jmnw.otp;

import me.jmnw.otp.util.BinaryUtils;

import java.io.File;

/**
 * bmessj (19.11.2014).
 */
public class OneTimePadDecoder {
    public File        key;
    public File        value;
    public File        decoded;
    public BinaryUtils binUtils_key;
    public BinaryUtils binUtils_value;
    public BinaryUtils binUtils_decoded;
    private long processedByteCount = 0;

    public OneTimePadDecoder(File key, File value, File decoded) {
        this.key = key;
        this.value = value;
        this.decoded = decoded;
        binUtils_key = new BinaryUtils(key, BinaryUtils.Mode.READ);
        binUtils_value = new BinaryUtils(value, BinaryUtils.Mode.READ);
        binUtils_decoded = new BinaryUtils(decoded, BinaryUtils.Mode.WRITE);
    }

    public OneTimePadDecoder(String key, String value, String decoded) {
        this(new File(key), new File(value), new File(decoded));
    }

    private byte decode(int key, int value) {
        processedByteCount++;
        if (processedByteCount % (1048576) == 0)
            System.out.println("Encoded: " + processedByteCount / 1048576 + " MiB");
        return (byte) (key + value);
    }

    public void decodeAll() {
        int bytes_key = binUtils_key.getByte();
        int bytes_value = binUtils_value.getByte();
        while (bytes_key > -1) {
            byte decoded = decode(bytes_key, bytes_value);
            binUtils_decoded.writeBytes(new byte[]{decoded});
            bytes_key = binUtils_key.getByte();
            bytes_value = binUtils_value.getByte();
        }

        binUtils_key.closeFile();
        binUtils_value.closeFile();
        binUtils_decoded.closeFile();
    }

}
