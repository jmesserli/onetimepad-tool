package me.jmnw.otp;

import java.io.File;
import java.util.Scanner;

/**
 * bmessj (19.11.2014).
 */
public class Main {

    public enum Mode {
        ENCODE, DECODE, BOTH
    }

    public static void encode(File input, File key, File value) {
        OneTimePadEncoder encoder = new OneTimePadEncoder(key, value, input);
        encoder.encodeAll();
    }

    public static void decode(File key, File value, File out) {
        OneTimePadDecoder decoder = new OneTimePadDecoder(key, value, out);
        decoder.decodeAll();
    }

    public static String getInput(String prompt, String standard) {
        System.out.printf("%s [%s]: ", prompt, standard);
        String input = new Scanner(System.in).nextLine();
        return input.isEmpty() ? standard : input;
    }

    public static void main(String[] args) {
        File input, key, value, out;

        String modeS = getInput("Encode / Decode / Encode + Decode (e / d / b)", "b");
        Mode mode = modeS.equals("e") ? Mode.ENCODE : modeS.equals("d") ? Mode.DECODE : Mode.BOTH;

        switch (mode) {
            case ENCODE:
                input = new File(getInput("Inputfile", "input.txt"));
                key = new File(getInput("Keyfile", "key.bin"));
                value = new File(getInput("Valuefile", "value.bin"));

                encode(input, key, value);
                break;
            case DECODE:
                key = new File(getInput("Keyfile", "key.bin"));
                value = new File(getInput("Valuefile", "value.bin"));
                out = new File(getInput("Outputfile", "out.txt"));

                decode(key, value, out);
                break;
            case BOTH:
                input = new File(getInput("Inputfile", "input.txt"));
                key = new File(getInput("Keyfile", "key.bin"));
                value = new File(getInput("Valuefile", "value.bin"));
                out = new File(getInput("Outputfile", "out.txt"));

                encode(input, key, value);
                decode(key, value, out);
                break;
        }
    }
}
